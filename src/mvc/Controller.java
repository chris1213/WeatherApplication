package mvc;

import constants.Constants;
import network.QueryUtils;
import parser.JSONParserClass;
import pojo.Weather;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static constants.Constants.BASIC_URL;
import static constants.Constants.ID;

public class Controller implements ActionListener {

    private JTextField searchTermTextField;
    private DefaultTableModel model;

    public Controller() {
    }

    public Controller(JTextField searchTermTextField, DefaultTableModel model) {
        super();
        this.searchTermTextField = searchTermTextField;
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String searchTerm = getTextFromUIField();

        if (isTextCorrect(searchTerm)) {

            QueryUtils queryUtils = new QueryUtils(new JSONParserClass());
            queryUtils.sendRequestAboutWeather(generatorUrl(searchTerm));
            Object object = queryUtils.takeTheResultLikeAObject();

            if (object != null){
                changeModel((Weather) object);
            }

        } else {
            JOptionPane.showMessageDialog(null,
                    "Search term is empty", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        model.setDataVector(Constants.DATA, Constants.TABLE_HEADER);
    }

    private void changeModel(Weather weatherObject) {

        Constants.DATA[0][0] = weatherObject.getName();
        Constants.DATA[0][1] = weatherObject.getTemperature();
        Constants.DATA[0][2] = weatherObject.getWeatherDescription();
    }

    private boolean isTextCorrect(String checkingText) {
        return checkingText != null && !"".equals(checkingText) && !checkingText.matches("[-+]?\\d*\\.?\\d+");
    }

    private String getTextFromUIField() {
        return searchTermTextField.getText();
    }

    private String generatorUrl(String textFromUI) {
        String location = "q=" + textFromUI;
        return BASIC_URL + location + ID;
    }
}
