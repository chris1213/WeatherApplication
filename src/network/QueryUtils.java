package network;

import org.json.JSONException;
import parser.IJSONParserClass;
import pojo.Weather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class QueryUtils {

    private IJSONParserClass ijsonParserClass;
    private String jsonResponse = "";

    public QueryUtils(IJSONParserClass ijsonParserClass) {
        this.ijsonParserClass = ijsonParserClass;
    }

    public void sendRequestAboutWeather(String urls) {

        URL url = createUrl(urls);
        try {
            HttpURLConnection httpURLConnection = makeHttpRequest(url);
            InputStream inputStream = getInputStream(httpURLConnection);
            jsonResponse = getJsonResponse(inputStream);
        } catch (IOException e) {
            System.out.println("Problem with making Http Request " + e);
        }
    }

    public Object takeTheResultLikeAObject(){
        return convertJsonToWeatherObject(jsonResponse);
    }

    private Weather convertJsonToWeatherObject(String jsonResponse) throws JSONException {
        return (Weather) ijsonParserClass.parseJSONToObject(jsonResponse);
    }

    private URL createUrl(String urls) {
        URL url = null;
        try {
            url = new URL(urls);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }

    private HttpURLConnection makeHttpRequest(URL url) throws IOException {

        if (url == null) {
            System.out.println("Error: URL is empty");
            return null;
        }
        HttpURLConnection urlConnection = null;

        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(8000);
            urlConnection.setConnectTimeout(10000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
        } catch (IOException e) {
            System.out.println("Error with connecting with server " + e);
        }
        return urlConnection;
    }


    private InputStream getInputStream(HttpURLConnection urlConnection){
        InputStream inputStream;
        try {
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
            } else {
                System.out.println("Error with connection");
                return null;
            }
        } catch (IOException e) {
            System.out.println("Error with inputStream: "+e);
            return null;
        }
        return inputStream;
    }

    private String getJsonResponse(InputStream inputStream){
        String jsonResponse;
        try {
            jsonResponse = convertStreamToString(inputStream);
        } catch (IOException e) {
            System.out.println("Error with converting Stream: "+e);
            return null;
        }
        return jsonResponse;
    }

    private String convertStreamToString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        StringBuilder response = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            response.append(line);
        }
        reader.close();
        return response.toString();
    }
}
