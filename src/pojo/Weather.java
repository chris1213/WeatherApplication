package pojo;


public class Weather {

    private String name;
    private long sunRise;
    private long sunSet;
    private double temperature;
    private String weatherDescription;

    public Weather(String name, long sunRise, long sunSet, double temperature, String weatherDescription) {
        this.name = name;
        this.sunRise = sunRise;
        this.sunSet = sunSet;
        this.temperature = temperature;
        this.weatherDescription = weatherDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSunRise() {
        return sunRise;
    }

    public void setSunRise(long sunRise) {
        this.sunRise = sunRise;
    }

    public long getSunSet() {
        return sunSet;
    }

    public void setSunSet(long sunSet) {
        this.sunSet = sunSet;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }
}
