package parser;

import org.json.JSONArray;
import org.json.JSONObject;
import pojo.Weather;

public class JSONParserClass implements IJSONParserClass{

    @Override
    public Object parseJSONToObject(String jsonResponse) {

        JSONObject basicJson = new JSONObject(jsonResponse);
        String name = basicJson.getString("name");
        JSONObject sys = basicJson.getJSONObject("sys");
        long sysSunrise = sys.getLong("sunrise");
        long sysSunset = sys.getLong("sunset");
        JSONObject main = basicJson.getJSONObject("main");
        double mainTemp = main.getDouble("temp");
        JSONArray weather = basicJson.getJSONArray("weather");

        String weatherDescription = "";
        for (int i = 0; i < weather.length(); i++) {
            JSONObject currentWeather = weather.getJSONObject(i);
            weatherDescription = currentWeather.getString("description");
        }

        return new Weather(name, sysSunrise, sysSunset, mainTemp, weatherDescription);
    }
}
