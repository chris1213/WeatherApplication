package parser;

public interface IJSONParserClass {
    Object parseJSONToObject(String jsonResponse);
}
