package mvc;

import org.junit.Before;
import org.junit.Test;
import pojo.Weather;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ControllerTest {

    private Controller privateObject;

    @Before
    public void createClassObject() {
        privateObject = new Controller();
    }

    @Test
    public void checkIfTextIsNotCurrect() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        Method privateStringMethod = Controller.class.
                getDeclaredMethod("isTextCorrect", String.class);

        privateStringMethod.setAccessible(true);

        privateStringMethod.invoke(privateObject, "Cracow");

    }

    @Test
    public void checkModelToChange() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        Method modelObject = Controller.class.
                getDeclaredMethod("changeModel", Weather.class);

        modelObject.setAccessible(true);

        modelObject.invoke(privateObject, new Weather(
                "name", 11, 12, 22, "Good"
        ));

    }


}